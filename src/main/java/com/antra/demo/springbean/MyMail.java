package com.antra.demo.springbean;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

@Component
public class MyMail {

	@Autowired
	JavaMailSender sender;

	@Autowired
	SimpleMailMessage msg;

	public void sendEmail() {

		msg.setFrom("From@gmail.com");
		msg.setTo("TO@gmail.com");
		msg.setText("Hai how are you");
		msg.setSubject("Test");

		sender.send(msg);
	}
}
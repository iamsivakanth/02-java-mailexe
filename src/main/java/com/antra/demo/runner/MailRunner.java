package com.antra.demo.runner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.antra.demo.springbean.MyMail;

@Component
public class MailRunner implements CommandLineRunner {

	@Autowired
	MyMail mail;

	@Override
	public void run(String... args) throws Exception {

		mail.sendEmail();
		System.out.println("Mail sent");
	}
}